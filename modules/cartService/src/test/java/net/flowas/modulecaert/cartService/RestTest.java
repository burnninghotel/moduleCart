package net.flowas.modulecaert.cartService;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import net.flowas.modulecart.domain.Category;

import org.junit.Test;

public class RestTest extends AbstractContainerTest {
	@Test
	public void test() throws Exception {
		Category category = new Category();
		category.setName("fff");
		Response rep = webTarget.path("/rest/shop/categories").request()
				.buildPost(Entity.json(category)).invoke();
		Category drep = rep.readEntity(Category.class);
		System.out.println(drep);
	}
}
