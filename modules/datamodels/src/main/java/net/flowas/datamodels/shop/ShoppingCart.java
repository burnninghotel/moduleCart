package net.flowas.datamodels.shop;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.Data;
@Data
@Entity
public class ShoppingCart extends IdAndDate{	       
        @OneToMany(mappedBy = "order")
	private Set<OrderItem>  items;
        @OneToMany
	private Set<Shipment>  shipments;	
}
