/**
 * 
 */
package net.flowas.datamodels.shop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import lombok.Data;

@Data
@Entity
public class Product extends IdAndDate{
    @NotNull
    private String code;
    @NotNull
    private String name; 
    private String description;
    private int ranking;
    @Past
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    private String detailsUrl;
    private String manufacturer;
    @OneToMany
    private List<ProductAttribute> attributes = new ArrayList<ProductAttribute>();
    @OneToMany
    private List<Product> constituentElements = new ArrayList<Product>();
    @OneToMany
    private List<ProductPrice> prices = new ArrayList<ProductPrice>();
    @OneToMany
    private List<ProductStock> stocks = new ArrayList<ProductStock>();
    @OneToMany
    private List<Retailer> retailers = new ArrayList<Retailer>();
    
}
